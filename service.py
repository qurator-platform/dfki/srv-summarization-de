from nltk.corpus import stopwords
import string
import os
from nif.annotation import *
import summarize


def analyzeNIF(nifDocument):
    d = nifDocument
    text = d.context.nif__is_string

    text = text.replace('\r\n', '\n')
    text = text.replace('\n', ' ')

    summary = analyzeText(text)
    # summary = "Summary of the text to be included in the NIF document."
    d.add_summary(summary)
    # kwargs = {"nif__summary" : summary}
    # nc = NIFContext(text,d.context.uri_prefix,**kwargs)
    # d2 = NIFDocument(nc, structures=d.structures)
    # return d2
    return d


def analyzeText(text):
    summary = summarize.APIMain(text)
    return summary


text: str = "Helene Fernau besuchte in Stettin das Lyzeum und Oberlyzeum und legte 1913 die Lehramtsprüfung und 1914 die Handarbeitslehrerinnenprüfung ab. Im Herbst 1915 nahm sie ein Philologiestudium an der Universität Berlin auf, welches sie in Greifswald beendete. Dort wurde sie mit ihrer Dissertation Der Monolog bei Hans Sachs promoviert.[1][2] Nach ihrem Studium unternahm sie Reisen in viele Länder, die bis nach Norwegen und Südamerika reichten, und vermittelte als Rezitatorin deutsche Kultur und Literatur. Auf einer ihrer Reisen lernte sie ihren Mann, einen Arzt aus Stuttgart, kennen. Ab 1925 lebte Helene Fernau-Horn fast 40 Jahre in Stuttgart.[2] In Stuttgart unterrichtete sie Gesang und Rezitation. Sie begann sich mit Stottern auseinanderzusetzen, als ihr Ärzte Patienten mit dieser Sprechstörung schickten. Da es bis dahin kaum Therapieansätze für Stottern gab, entwickelte sie in Zusammenarbeit mit Ärzten eigene Therapiekonzepte für die Behandlung von Stimmstörungen und Stottern. Während des Zweiten Weltkriegs ruhte ihre Praxis. Von 1947 bis 1958 bildete sie in ihrer eigenen Praxis die ersten Logopädinnen in Süddeutschland aus und leitete eine Ambulanz am Katharinenhospital Stuttgart.[3][4] Sie leistete auch praktische sprecherzieherische Arbeit, für die sie aus ihrer Erfahrung als Rezitatorin schöpfen konnte.[2] 1956 veröffentlichte sie ihr Konzept für die Atemwurf-Methode, deren Ansatz die Atemfunktion und Artikulation ist.[5] Sie verfasste insgesamt 26 Arbeiten, von denen sich 17 mit dem Stottern befassen, darunter ihre Monographie Die Sprechneurosen, die in vielen Sprachheilschulen Anwendung fand. Ihr Ansatz beruhte auf dem Aufbau eines Ruhe- und Ablaufzirkels und der Umstellung einer pathologischen Hochatmung auf eine physiologische Zwerch­fellflankenatmung. Für ihre Methodik waren zudem Sprechübungen in einem nuancierten „Formel­training“ charakteristisch.[2] Ein weiteres Werk zur Störung der Stimme konnte sie nicht mehr fertigstellen.[4] Fernau-Horns Einsatz für die Phoniatrie und Logopädie wurde Mitte der siebziger Jahre mit zahlreichen offiziellen Anerkennungen gewürdigt.[2][3] 1969, nach dem Tod ihres Mannes, verlegte Fernau-Horn ihren Lebensmittelpunkt nach München. Sie starb 1975 in Ruhpolding. Ruth Dinkelacker, eine der Schülerinnen Fernau-Horns, erweiterte den Ansatz der Atemwurf-Methode zu einer eigenen Stimmtherapie."
