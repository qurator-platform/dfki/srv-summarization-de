import os


class Config(object):
    """Base configuration for this application.

    Contains information about the current environment, different levels of logging, the database,
    and hosting options. The values are shared with all subclasses (if not overridden)."""
    ROOT_DIRECTORY = os.path.abspath(".")
    SUMMARIZATION_GERMAN_SERVICE = "summarization-german-service"
    DOCKER_IMAGE = f"konstantinschulz/{SUMMARIZATION_GERMAN_SERVICE}:v1"
    DOCKER_PORT = 8000
    HOST_PORT = 8181
    MODEL_NAME = "./distilbert-base-multilingual-cased"
