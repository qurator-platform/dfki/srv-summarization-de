from typing import Any

from config import Config
from elg import FlaskService
from elg.model import TextsResponse, TextsResponseObject

from service import analyzeText


class SummarizationGermanService(FlaskService):

    def convert_outputs(self, content: str) -> TextsResponse:
        return TextsResponse(texts=[TextsResponseObject(content=analyzeText(content))])

    def process_text(self, content: Any) -> TextsResponse:
        return self.convert_outputs(content.content)


sgs: SummarizationGermanService = SummarizationGermanService(Config.SUMMARIZATION_GERMAN_SERVICE)
app = sgs.app
