from typing import Any
from config import Config
import time
import docker
from docker.models.containers import Container
from elg.model import TextRequest, TextsResponse
import unittest
from elg import Service

from elg_service import SummarizationGermanService


class ElgTestCase(unittest.TestCase):
    content: str = "Albert is from Germany."

    def test_local(self):
        service = SummarizationGermanService(Config.SUMMARIZATION_GERMAN_SERVICE)
        # request: {"type": "text", "params": null, "content": "Albert is from Germany.", "mimeType": "text/plain", "features": null, "annotations": null}
        request = TextRequest(content=ElgTestCase.content)
        response: TextsResponse = service.process_text(request)
        self.assertTrue(response.texts[0].content.startswith("Albert ist ein deutscher Entwickler"))
        self.assertEqual(type(response), TextsResponse)

    def test_docker(self):
        client = docker.from_env()
        ports_dict: dict = dict()
        ports_dict[Config.DOCKER_PORT] = Config.HOST_PORT
        container: Container = client.containers.run(
            Config.DOCKER_IMAGE, ports=ports_dict, detach=True)
        # wait for the container to start the API
        time.sleep(1)
        service: Service = Service.from_docker_image(
            Config.DOCKER_IMAGE,
            f"http://0.0.0.0:{Config.DOCKER_PORT}/process", Config.HOST_PORT)
        response: Any = service(ElgTestCase.content, sync_mode=True)
        cr: TextsResponse = response
        container.stop()
        container.remove()
        self.assertTrue(cr.texts[0].content.startswith("Albert ist ein deutscher Entwickler"))
        self.assertEqual(type(cr), TextsResponse)

    def test_elg_remote(self):
        service = Service.from_id(7348)
        response: Any = service(ElgTestCase.content)
        cr: TextsResponse = response
        self.assertTrue(cr.texts[0].content.startswith("Albert ist ein deutscher Entwickler"))
        self.assertEqual(type(cr), TextsResponse)


if __name__ == '__main__':
    unittest.main()
