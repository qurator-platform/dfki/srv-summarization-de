#!/usr/bin/python3
from flask import Flask, flash, request, redirect, url_for

from nif.annotation import *

from flask_cors import CORS
import re
import os
import shutil
from werkzeug.utils import secure_filename
import zipfile
import sys
import dill as pickle
import codecs
import service
import json
"""

then to start run:
export FLASK_APP=flaskController.py
export FLASK_DEBUG=1 (optional, to reload upon changes automatically)
python -m flask run

example calls:
curl -X GET localhost:5000/welcome

"""


app = Flask(__name__)
app.secret_key = "super secret key"



CORS(app)

@app.route('/srv-summarize/analyzeText', methods=['POST'])
def summarize():
    #"""
    if request.method == 'POST':
        cType = request.headers["Content-Type"]
        accept = request.headers["Accept"]
        data=request.stream.read().decode("utf-8")
        if accept == 'text/turtle':
            pass
        elif accept == 'text/plain':
            pass
        else:
            return 'ERROR: the Accept header '+accept+' is not supported!'
        if cType == 'text/plain':
            if accept == 'text/turtle':
                uri_prefix="http://lynx-project.eu/res/"+str(uuid.uuid4())
                d = NIFDocument.from_text(data, uri_prefix)
                summ = service.analyzeNIF(d)
                return summ.serialize(format="ttl")
            elif accept == 'text/plain':
                summ = service.analyzeText(data)
                return summ
            else:
                return 'ERROR: the Accept header '+accept+' is not supported!'
        elif cType == 'text/turtle':
            d = NIFDocument.parse_rdf(data, format='turtle')
            if accept == 'text/turtle':
                summ = service.analyzeNIF(d)
                return summ.serialize(format="ttl")
            elif accept == 'text/plain':
                datatxt = d.context.nif__is_string
                summ = service.analyzeText(datatxt)
                return summ
            else:
                return 'ERROR: the Accept header '+accept+' is not supported!'
        else:
            return 'ERROR: the contentType header '+cType+' is not supported!'

        ##annotatedNIF = service.analyzeNIF(d)
        #summ = service.analyzeNIF(d)
        ##return annotatedNIF.serialize(format="ttl")
        ##return summ.serialize(format="ttl")
        #if accept == 'text/plain':
        #    return d.context.__getattr__('nif__summary')
        #elif accept == 'text/turtle':
        #    return summ.serialize(format="ttl")
    else:
        return 'ERROR, only POST method allowed.'

    #"""
    """
    # @ Ela: if you comment out the stuff above and uncomment the lines below, it works using parameters instead of headers. Note that it just always spits out turtle, only works with plaintext (txt) as input, so it's dummy params still, but we can worry about that after the demo.
    supported_informats = ['txt']
    supported_outformats = ['turtle', 'txt']
    inp = None
    if request.args.get('input') == None:
        if request.data == None:
            return 'Please provide some text as input.\n'
        else:
            inp = request.data.decode('utf-8')
    else:
        inp = request.args.get('input')
    if request.args.get('outformat') == None:
        return 'Please specify output format (currently supported: %s)\n' % str(supported_outformats)
    elif request.args.get('outformat') not in supported_outformats:
        return 'Output format "%s" not among supported formats. Please picke one from: %s.\n' % (request.args.get('outformat'), (str(supported_outformats)))
    if request.args.get('informat') == None:
        return 'Please specify input format (currently supported: %s)\n' % str(supported_informats)
    elif request.args.get('informat') not in supported_informats:
        return 'Input format "%s" not among supported formats. Please picke one from: %s.\n' % (request.args.get('informat'), (str(supported_informats)))
   
    
    informat = request.args.get('informat')
    outformat = request.args.get('outformat')
    if outformat == 'turtle':
        uri_prefix="http://lynx-project.eu/res/"+str(uuid.uuid4())
        d = NIFDocument.from_text(inp, uri_prefix)
        
        summ = service.analyzeNIF(d)
        return summ.serialize(format="ttl")
    elif outformat == 'txt':
        summ = service.analyzeText(inp)
        return summ
    """

def create_error(message):
    failure_response_dict = {"failure":{"errors":[message]}}
    return json.dumps(failure_response_dict)

@app.route('/', methods=['POST'])
def summarizeELG():
    warnings = []
    supported_mymetypes = ['text/plain','text/turtle']
    if request.method == 'POST':
        cType = request.headers["Content-Type"]
        accept = request.headers["Accept"]
        #data=request.stream.read().decode("utf-8")
        #print(data)
        #print("NEW LINE")
        #print(request.stream.read())
        if request.data == None:
            return create_error("Error: the mimeType is not supported!")
        #content = request.stream.read().decode("utf-8")
        request_body = request.data.decode('utf-8')
        request_dict = json.loads(request_body)
        mimeType = request_dict['mimeType']
        print(mimeType)
        if mimeType=='text/plain':
            content = request_dict['content']
        else:
            return create_error("Error: the mimeType is not supported!")
        print("DEBUG: ", content)
        if accept == 'application/json':
            pass
        else:
            return create_error('ERROR: the Accept header '+accept+' is not supported!')

        if cType == 'application/json':
            summ = service.analyzeText(content)
            #d = NIFDocument.parse_rdf(data, format='turtle')
            #annotatedNIF = service.analyzeNIF(d)
            #result = ner.predict(inp, informat, outformat)
            #ner_annotations = ner.predictELG(content)
            #annot_dict = {
            #  "start":0,
            #  "end":6,
            #  "features":{
            #    "itsrdf:taClassRef": "dbo:Location",
            #    "itsrdf:taIdentRef": "dbo:Berlin",
            #  }
            #}
            #ner_annotations.append(annot_dict)
            response_dict = {
              "response":{
                "type":"texts",
                "warnings":warnings,
                "texts":[
                  {
                    "role":"string", 
                    "content":summ,
                    "features":{ "text_type": "summary" }
                  }
                ]
              }
            }
            #response_dict['response']['annotations']['ner'] = ner_annotations
            return json.dumps(response_dict)
        else:
            return create_error('ERROR: the contentType header '+cType+' is not supported!')
    else:
        return create_error('ERROR, only POST method allowed.')

    
if __name__ == '__main__':
    port = int(os.environ.get('PORT',8080))
    app.run(host='localhost', port=port, debug=True)
